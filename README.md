# The Arctic climate at 1.5 °C and 2.7 °C global warming

## Description
This repository provides the code used to produce Figures 1 and 2 of Stroeve et al., _Disappearing landscapes: The Arctic at +2.7 °C global warming_, Science (in preparation). A detailed description of the analysis is provided in the methods section of the paper. The repository contains four Jupyter notebooks for the analysis of Arctic surface air temperature (Arctic_SAT.ipynb, Figure 1), Arctic sea-ice area (Sea-ice.ipynb, Figure 2a), Greenland melt days (Greenland_meltdays.ipynb, Figure 2b), and permafrost area (Permafrost.ipynb, Figure 2c).

## Contact
For any questions regarding the code and analysis, feel free to contact the author Céline Gieße (celine.giesse@uni-hamburg.de). Post-processed data from climate models used in the analysis are available upon request.